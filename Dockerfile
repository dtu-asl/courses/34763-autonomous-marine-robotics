# Use the base image
FROM tiryoh/ros-desktop-vnc:noetic-20250112T0417

# Set the default URL for conda installation (overwritten by the build scripts)
ARG CONDA_URL=https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh

# Install ROS dependencies
RUN apt-get update && apt-get install -y \
    python3-pip \
    # This somehow is not installed using rosdep
    ros-noetic-realsense* \ 
    && rm -rf /var/lib/apt/lists/*

# Install (downgrade) specific versions of python packages needed for UUV simulator
RUN pip3 install scipy numpy==1.19

# Install miniconda

ARG PATH="/root/miniconda3/bin:${PATH}"
ENV PATH="/root/miniconda3/bin:${PATH}"
RUN wget $CONDA_URL \
    && mkdir /root/.conda \
    && bash $(basename $CONDA_URL) -b \
    && rm -f $(basename $CONDA_URL)