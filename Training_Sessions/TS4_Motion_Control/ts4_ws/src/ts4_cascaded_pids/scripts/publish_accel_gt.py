#!/usr/bin/env python

import rospy
from geometry_msgs.msg import TwistStamped
from nav_msgs.msg import Odometry

class Publish_Acceleration_Ground_Truth:
    def __init__(self):
        # Initialize the node
        rospy.init_node('publish_accel_gt', anonymous=True)
    
        # Get the UUV name parameter (default to 'bluerov1')
        uuv_name = rospy.get_param('~uuv_name', 'bluerov1')

        # Subscriber to the velocity topic
        self.velocity_sub = rospy.Subscriber(f'/{uuv_name}/pose_gt', Odometry, self.velocity_callback)

        # Publisher for acceleration
        self.acceleration_pub = rospy.Publisher(f'/{uuv_name}/accel_gt', TwistStamped, queue_size=10)

        # Variables to store the previous velocity and timestamp
        self.prev_velocity = None
        self.prev_time = None

    def velocity_callback(self, msg):
        current_time = msg.header.stamp  # Get the current ROS time in seconds
        msg = msg.twist.twist
        
        # Calculate linear velocity components
        current_linear_velocity = msg.linear
        current_angular_velocity = msg.angular

        # Initialize acceleration message
        acceleration_msg = TwistStamped()

        # Calculate acceleration if there is a previous velocity value
        if self.prev_velocity is not None and self.prev_time is not None:
            delta_time = (current_time - self.prev_time).to_sec()
            print(current_time, delta_time)
            if delta_time > 0:  # Avoid division by zero
                acceleration_msg.header.stamp = current_time
                acceleration_msg.twist.linear.x = (current_linear_velocity.x - self.prev_velocity['linear'].x) / delta_time
                acceleration_msg.twist.linear.y = (current_linear_velocity.y - self.prev_velocity['linear'].y) / delta_time
                acceleration_msg.twist.linear.z = (current_linear_velocity.z - self.prev_velocity['linear'].z) / delta_time
                
                acceleration_msg.twist.angular.x = (current_angular_velocity.x - self.prev_velocity['angular'].x) / delta_time
                acceleration_msg.twist.angular.y = (current_angular_velocity.y - self.prev_velocity['angular'].y) / delta_time
                acceleration_msg.twist.angular.z = (current_angular_velocity.z - self.prev_velocity['angular'].z) / delta_time

                # Publish the acceleration
                self.acceleration_pub.publish(acceleration_msg)

        # Update the previous velocity and time
        self.prev_velocity = {
            'linear': current_linear_velocity,
            'angular': current_angular_velocity
        }
        self.prev_time = current_time

    def run(self):
        rospy.spin()

if __name__ == '__main__':
    try:
        node = Publish_Acceleration_Ground_Truth()
        node.run()
    except rospy.ROSInterruptException:
        pass
