num = [0 0.1153];
den = [1 0.4488]; 

yaw_ref = deg2rad(90);

mass = 8.53;
x_size = 0.53;
y_size = 0.33;
Izz = 0.2*mass*(x_size*x_size + y_size*y_size);
accel_to_Mz = Izz;

% num = [1];
% den = [Izz 0];

% num = num*accel_to_Mz;

velocity_limit = 0.6; % rad per secs
Mz_limit = 11; % Nm

Kp_pos = 10;
Ki_pos = 0.1;
Kd_pos = 0.0001;

Kp_vel = 100;
Ki_vel = 0.1;
Kd_vel = 0.0001;

simout = sim("Cascaded_PID_tuning_yaw_motion");

figure;
hold on;
plot(simout.ref_pos,"r");
plot(simout.y_pos,"b");
legend("ref_{pos}","y_{pos}");
grid on;
hold off;

figure;
hold on;
plot(simout.ref_vel,"r");
plot(simout.y_vel,"b");
legend("ref_{vel}","y_{vel}");
grid on;
hold off;

figure;
hold on;
plot(simout.u_acc,"b");
plot(simout.Mz,"r");
legend("u_{acc}","Mz");
grid on;
hold off;