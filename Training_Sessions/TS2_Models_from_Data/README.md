## Training Session 2

### Exercise 1

1. Open `Training_session_2_Model_learning_Heading.mlx` using MATLAB.
2. Run each cell to visualize data obtained from testing a BlueROV1 registered on the motion capture system in ASTA.
3. After reaching the "Data for model learning" section. You should select a portion of data for model identification (learning) by dragging a box and double-clicking inside it.

   ![System Identification Learning](./media/system_identification_learning.png)

4. Repeat the process for model validation (assessment).

   ![System Identification Validation](./media/system_identification_validation.png)

5. The System Identification GUI will appear.
6. Import the data to be identified: `Import data` -> `Time domain data`.
7. Fill in Input, Output, and Data Name -> `Import` -> `Close`.

   ![Import Identification Data](./media/sysid_import_ident.png)

8. Import validation data following steps 6-7 for validation.

   ![Import Validation Data](./media/sysid_import_valid.png)

9. Drag and drop the `valid_Data` into the right Validation Data box.

10. Start estimating the model: `Estimate` -> `State-space models` -> Set the model order -> `Estimate`.

    ![Estimate Model](./media/sysid_estimate_model.png)

11. Drag the estimated model result (`ss1`) to the "To Workspace" box.

12. View the state-space models and obtain the $\alpha$ and $\beta$ parameters of the first-order Nomoto model:

    ```matlab
    present(ss1)
    ```

### Exercise 2

Implement the remaining code in `Training_session_2_Model_learning_Steering.mlx` using the approach and methods from Exercise 1 as reference.

### **Exercise 3**

#### **I. Autonomous Marine Vehicle Without Ocean Current**
An autonomous marine vehicle is sailing with a constant speed of $U = 10$ knots and a constant heading $\phi = -25^\circ$. Assuming the motion is constrained to the horizontal plane and no ocean current is present, perform the following steps:

1. **Derive** the continuous-time kinematic model describing the motion of the autonomous marine vehicle in the tangential frame.
2. **Implement** the derived model in MATLAB/Simulink.
3. **Determine** the position of the craft in the tangential frame at $t = 250s$, given the initial conditions:
   - $N_0 = 1000m$
   - $E_0 = -35m$
   - $\phi_0 = -25^\circ$
4. **Plot** the trajectory of the marine craft in the tangential frame.

#### **II. Autonomous Marine Vehicle With Ocean Current**
Now, assume a constant ocean current is present in the operating area. The current velocity is $V_c = 1.5$ m/s with a direction of $\beta_c = 65^\circ$. Perform the following:

1. **Modify** the continuous-time kinematic model to account for the ocean current.
2. **Adapt** the MATLAB/Simulink model to include the ocean current effect.
3. **Determine** the position of the craft at $t = 250s$, using the modified model (repeat **I.3**).
4. **Plot** the updated trajectory (repeat **I.4**).

#### **III. Remotely Operated Vehicle (ROV) Descending in a Helix**
A remotely operated vehicle (ROV) descends along a helical path with:
- **Helix radius:** $a = 5m$
- **Pitch:** $2\pi b$, where $b = 0.25m/s$
- **Descent time:** 240 seconds  

Using this information, perform the following:

1. **Derive** the continuous-time kinematic model describing the motion of the ROV in the tangential frame.
2. **Implement** the derived model in MATLAB/Simulink.
3. **Determine** the position of the ROV at $t = 120s$ and at the end of the descent, given the initial conditions:
   - $N_0 = 0m$
   - $E_0 = 100m$
   - $D_0 = 10m$
   - $\phi_0 = 90^\circ$
4. **Plot** the trajectory of the ROV in the tangential frame.

For more information on helices: [Helix - Wikipedia](https://en.wikipedia.org/wiki/Helix).

### **Exercise 4 - Logging ROS Data Simulation**

In this exercise, one has to record ROV data from the ROS simulation to then perform system identification on the recorded data.

1. **Collect the data**

   Run the linux container and open three terminal windows. On the first one, launch the simulation: 

   ```bash
   roslaunch bluerov2_gazebo start_pid_demo.launch
   ```
   On the second one, run the command script that was build during Training Session 1:
   ```bash
   rosrun ts1_simple_command square_wave_surge_command.py
   ```
   Make sure this script is compiled and sourced. On the third one, collect the data with in a ROS bag file:
   ```bash
   rosbag record -a
   ```
   - After terminating the recording with `Ctrl+C`, the `.bag` file will be saved in the current directory.
   - To record only specific topics, use:
     ```bash
     rosbag record <topic1> <topic2> ... <topicN>
     ```
   - More information on `rosbag` can be found at: [wiki.ros.org/rosbag/Commandline](http://wiki.ros.org/rosbag/Commandline).

2. **Use the recorded `.bag` file** for system identification analysis.
