# 34763 - Autonomous Marine Robotics

This is the main repo for the DTU course 34763 Autonomous Marine Robotics.

The repository is structured as follows:


    ├── .gitignore         <- ignore file for git, ensures that we only store files we care about
    ├── Dockerfile         <- Dockerfile which contains a ROS noetic installation
    ├── README.md          <- The top-level README for developers using this project.
    │
    ├── media/             <- Contains pretty pictures for the README.md
    |
    ├── ros_ws/            <- Main folder for the ROS source code and packages used in the course
    │   └── src/            
    │       ├── uuv_simulator  <- Underwater simulator package
    │       ├── bluerov2       <- Main ROS package for the BlueROV, contains the controllers, transforms, etc.
    │       ├── ....
    │       └── bluerov2_gazebo <- BlueROV Gazebo packages
    │
    ├── scripts/           <- General scripts
    │   ├── activate_conda.sh                   <- Enables a conda python environment inside the container
    │   ├── build_and_create_docker.sh          <- Builds the image and creates a container
    │   ├── windows_build_and_create_docker.sh  <- Builds the image and creates a container
    │   ├── setup_ros_environment.sh            <- Installs ROS dependencies inside the container
    │   └── start_container.sh                  <- Starts the container
    │
    └── Training_Sessions/   <- Folders of exercise material for each lecture, and the exercise manual in a README.md
        ├── TS1_Introduction_to_Simulation
        ├── TS2_Models_from_Data
        ├── TS3_Path_Planning
        ├── TS4_Motion_Control
        ├── TS5_Mission_Planning
        └── TS6_Perception

To get started, this readme will cover the following steps:

1. [Downloading and setting up git repo](#downloading-the-code)
2. [Installing Docker and build Container](#docker-setup)
3. [Starting the container](#starting-the-docker-container)
4. [Frequently Asked Questions](#FAQ)
5. [Getting Extra Help](#getting-help)


## Downloading the code

First of all download or clone this repository to your own machine.

Note: For Windows PC users, please execute this before cloning the repository:

```bash
git config --global core.autocrlf false
```

To clone a copy of the repository (repo) onto your local machine, open a terminal on you machine, and run the command:

```bash
git clone https://gitlab.gbar.dtu.dk/dtu-asl/courses/34763-autonomous-marine-robotics.git
```

The repo contains some external code dependencies, which are included as submodules, and these needs to be downloaded as well.
In order to download the submodules, run the command

```bash
cd 34763-autonomous-marine-robotics
```

to change the directory to the course repo (enter into the folder).

## Docker Setup

### Pre-install checks

Docker is a virtualisation software, which requires that your machine is setup to enable virtualisation.
This is something that you might have to enable in the BIOS of your machine if it is not already enabled.
How to access the BIOS differs from make and model, but if you are unsure of how to access the BIOS and enable virtualisation on your machine, you can google the following: 

    how to enable virtualization in bios <model and make of laptop>

For example, for a HP elitebook G6

    how to enable virtualization in bios HP elitebook G6


### Installing Docker


Before building the ROS Docker image, make sure you have Docker installed on your system. If not, follow the instructions below to install Docker:

1. **Linux:** [official linux guide](https://docs.docker.com/desktop/install/linux-install/)
2. **Windows:** [official windows guide](https://docs.docker.com/desktop/install/windows-install/)
3. **Mac:** [official MacOS guide](https://docs.docker.com/desktop/install/mac-install/)

### Creating the Docker container

Now we will create a docker container that will be used throughout the course, i.e., it will/should persist (live) untill you are done with the course.
However, the container can be started and stopped whenever you wish, and all your progress will be saved, so long you do not delete the container (this is why we created a fork before, we have our own personal backup!).

Open a terminal and navigate to the root folder of the cloned repository:

```bash
   cd 34763-autonomous-marine-robotics/
```

Now we need to
   1. Build the docker image (takes a while)
   2. Create a container based on the built image

The repo contains scripts which performs both of the above tasks, please see instructions below for you specific system.

#### Building on Windows

To build and create the docker container on Windows, please follow this [guide](/windows_build_guide.md).

#### Building on Linux/MacOS

Open a terminal and run the following command:

```bash
./scripts/build_and_create_docker.sh
```

If you get `permission denied` error, use `sudo` as such

```bash
sudo ./scripts/build_and_create_docker.sh
```


### Starting the Docker container

Depending on your installation you can start the container is a few ways

- **Commandline**:  
   To run the Docker container and access it through VNC, execute the following command:

   ```bash
   ./scripts/start_container.sh
   ```
   If you get `permission denied` error, use `sudo` as such
   ```bash
   sudo ./scripts/start_container.sh
   ```
   The terminal should print a lot of stuff and should remain open.  
   Stopping the container is just a matter of pressing `CTRL + C` in the same terminal.

- **Using Docker Desktop**:

   Click the small :arrow_forward: button under the `Actions` tab on the right (visible in the image above).  
   To stop the container again, press the :white_square: under the `Actions` tab.

Once the Docker container is running, you can access the VNC server through your web browser using the following URL:

[http://localhost:6080/](http://localhost:6080/)

### Post Container Creation

Now that we have a freshly created container, we need to install some extra ROS dependencies and build the entire simulation framework.
All of this is done for you by the `scripts/setup_ros_environment.sh` script, which we will have to run as the first thing upon creating a new container. 

> **IMPORTANT**  
> The `setup_ros_environment.sh` should only be run *ONCE* in a freshly created container, so only run this when/if ever you need to re-create the container!

Open the `Terminator` application located on the desktop inside the container.

Execute the following command:

```bash
source /home/ubuntu/34763-autonomous-marine-robotics/scripts/setup_ros_environment.sh
```

This script will install all the required dependencies inside the container. The script will inform you of what is running, however, the process might take a while, so please be patient.

Once the script finishes, you are ready for the course.

### Testing the ROS Installtion

To test the simulation software, open a terminal (Terminator application on the desktop) and run:

```bash
roslaunch bluerov2_gazebo start_pid_demo.launch
```

This should start `RViz` (ros visualisation tool) and `Gazebo` (simulator) with the BlueROV underneath a bridge.
If you cannot see the BlueROV, you can open the `Models` dropdown menu in the Gazebo window, right-click on the `bluerov2` model (as shown below), and select `follow`, this will keep the BlueROV in focus.

![](./media/gazebo_follow_bluerov.png)

## FAQ

We've added some answers to frequently asked questions below, please consult this section before asking for assistance.

-  **I experience low framerate in the gazebo simulator?**:

   The simulator *is* slow, however, you can disable shades in the `Scenes` overlay, and also disable simulation of wind in the `Weather` overlay, to add some FPS.

- **The `build_and_create_script.sh` failed?**

   The building process may be failed if you have the container created before. To solve that, you need to remove it. 
   You should remove the container by the Docker Desktop:

   ![](./media/docker_desktop_remove_container1.png)
   ![](./media/docker_desktop_remove_container2.png)

   Then re-run the build script to re-create the container and re-run the `setup_ros_environment.sh` script inside the container.

- **Error in a file contains "\r"?**

   If you are getting the below error:  
   ![](./media/error_crlf_lf.png)

   **Cause:** This error can occur for Windows users, if they did not set the git global configuration mentioned in the top of this guide, which causes windows to change the line endings of the files, which will cause the linux python interpreter inside the contianer to fail.
   
   **Solution:**
   1. Find the related file (green underlined)
   2. Open with Visual Studio Code
   3. Click CRLF at the bottom right of Visual Studio Code
   4. Change it to LF
   5. Save


## Getting Help

If you need help outside of alloted time for exercises, there are several ways:

First, there is an associated teams group for the course, which you have automatically been added too ([link](https://teams.microsoft.com/l/team/19%3AzTda46QHGOWLLjD-uQFvtURCm4AHDqWxTx0kbYinAvo1%40thread.tacv2/conversations?groupId=aa7e5dc1-587e-4389-913b-6fb307a5462f&tenantId=f251f123-c9ce-448e-9277-34bb285911d9)).  
Here you can ask questions and get feedback from your fellow students, and the TA's will be available online on Fridays from 09:00-10:00.

Secondly, we have office hours which needs to be booked atleast 1 hour in advance through the links provided below.

   - Nicholas: Wednesdays from 09:30 - 11:00 (booking [link](https://outlook.office.com/bookwithme/user/cd0db2e3872849f49a088f1fa4c82ad7@dtu.dk/meetingtype/2agjfoHghE2rt6rr6RMHNQ2?anonymous&ep=mlink))
   - Yaqub: Mondays from 10:00 - 11:30 (booking [link](https://outlook.office.com/bookwithme/user/64c87bb63fdc4d4ea2c895e610ea305f%40dtu.dk/meetingtype/e444b2e2-640e-43f3-a316-838fc1595bc0?anonymous))
   - Morgan: Friday from 9:30 - 11:00 (booking [link](https://outlook.office.com/bookwithme/user/046c86d97f134285920d5902e2841b5b@dtu.dk/meetingtype/xdMkX3_haE-Er1teLc4RcA2?anonymous&ep=mlink))


## Contributing

Please feel free to submit merge (pull) requests with your suggested changes, please make sure that you either tag or assign the request to Nicholas (@pnha) or Yaqub (@yaqpr), this way you ensure that we are notified via email.
