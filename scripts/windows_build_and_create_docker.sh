#!/bin/bash

CURRENT_DIR=$(pwd -W)

echo "\033[33mBStarting the build process, this might take a while...\033[0m"

# Set the x86_64 platform
PLATFORM_FLAG="--build-arg CONDA_URL=https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh"

# Default variables
NO_CACHE_FLAG=""

# Parse command-line arguments
while [[ "$#" -gt 0 ]]; do
    case $1 in
        --no-cache) NO_CACHE_FLAG="--no-cache" ;;
        -h|--help) 
            echo "Usage: $0 [--no-cache]"
            echo "  --no-cache   Build the Docker image without using cache"
            exit 0
            ;;
        *)
            echo "\033[31mUnknown argument: $1\033[0m"
            echo "Use -h or --help for usage information"
            exit 1
            ;;
    esac
    shift
done

# Build the Docker image
echo -e "\033[33mBuilding the Docker image with the following settings:\033[0m"
echo -e "\033[33m  Architecture: $ARCHITECTURE\033[0m"
echo -e "\033[33m  No cache: $NO_CACHE_FLAG\033[0m"

docker build -t dtu_34763_ros_image $PLATFORM_FLAG $NO_CACHE_FLAG -f Dockerfile .

# Create the container
echo -e "\033[33mCreating container...\033[0m"
docker create \
    -p 6080:80 \
    --shm-size=2048m \
    --privileged \
    -v "${CURRENT_DIR}:/home/ubuntu/34763-autonomous-marine-robotics/:rw" \
    --name dtu_34763 dtu_34763_ros_image

echo -e "\033[32mContainer created! Run the 'scripts/start_container.sh' script to start the container.\033[0m"
