#!/bin/bash

echo -e "\033[33mGetting the latest package metadata...\033[0m"
sudo apt-get update 

echo -e "\033[33mMoving to ros_ws folder and installing dependencies\033[0m"
cd /home/ubuntu/34763-autonomous-marine-robotics/ros_ws && rosdep install --from-path src --ignore-src -r -y

echo -e "\033[33mBuilding the ros_ws...\033[0m"
catkin build
echo -e "\033[33mWorkspace built.\033[0m"

# Define the sourcing line
SOURCE_LINE="source /home/ubuntu/34763-autonomous-marine-robotics/ros_ws/devel/setup.bash"

# Check if the line already exists in .bashrc
if grep -Fxq "$SOURCE_LINE" ~/.bashrc; then
    echo -e "\033[33mSourcing line already exists in .bashrc. Skipping addition.\033[0m"
else
    echo -e "\033[33mAdding sourcing line to .bashrc...\033[0m"
    echo "$SOURCE_LINE" >> ~/.bashrc
    echo -e "\033[32mSourcing line added.\033[0m"
fi

# Source .bashrc to apply changes immediately (optional)
echo -e "\033[33mSourcing .bashrc...\033[0m"
source ~/.bashrc
echo -e "\033[32mSetup complete.\033[0m"

cd /home/ubuntu/34763-autonomous-marine-robotics/