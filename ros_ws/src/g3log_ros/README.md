# g3log_ros pkg

Catkin + ROS wrapper for [g3log](https://github.com/KjellKod/g3log)

We use g3log for all of our C++ ROS-agnostic libraries. This wrapper adds sinks that redirect the logs to appropriate roslog streams.

## Usage

### non-ROS library code

Use g3log like normal.
~~~cpp
#include <g3log/g3log.hpp>

int foo = 42;
LOG(WARNING, "This goes to ROS_WARN_STREAM! foo = " << foo);
LOG(DEBUG, "This goes to ROS_DEBUG_STREAM!");
LOG(INFO, "This (and any other log levels) goes to ROS_INFO_STREAM");
~~~

### ROS wrapper / main

* Add the appropriate includes to your node's main:
~~~cpp
#include "g3log_ros/ROSLogSink.h"
#include "g3log_ros/g3logger.h"
~~~
* Initialize the logger (typically just after `ros::init`):
~~~cpp
libg3logger::G3Logger<ROSLogSink> log_worker("oculus_node");
~~~
